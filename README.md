[![pipeline status](https://gitlab.com/thorchain/trade-bots/asgard-bot/badges/master/pipeline.svg)](https://gitlab.com/thorchain/trade-bots/asgard-bot/-/commits/master)
[![coverage report](https://gitlab.com/thorchain/trade-bots/asgard-bot/badges/master/coverage.svg)](https://gitlab.com/thorchain/trade-bots/asgard-bot/-/commits/master)

Asgard-bot
==========

Asgard-bot is an automated market maker, trading Binance tokens between
THORChain and Binance DEX.

### Status
In development, not ready for use.
