import os
import logging
import json

from clients.binance import Binance
from clients.binance_dex import BinanceDex
from clients.thorchain import Thorchain

from util.common import Coin, Asset

from binance_chain.http import HttpApiClient
from binance_chain.wallet import Wallet
from binance_chain.environment import BinanceEnvironment

BNB = Asset("BNB.BNB")
RUNE = Asset("BNB.RUNE-B1A")


class Transit:
    def __init__(self, thorchain_address, thorchain):
        self.thorchain_address = thorchain_address
        self.thorchain = Thorchain(thorchain)

        env = BinanceEnvironment.get_production_env()
        self.client = HttpApiClient(env=env)
        self.binance = Binance()
        self.binance_dex = BinanceDex()
        self.wallet = Wallet.create_wallet_from_mnemonic(
            os.environ.get("MNEMONIC"), env=env
        )

    def run(self, instructions):
        for i in instructions:
            coin = Coin(i["in"]["denom"], i["in"]["amount"])
            if i["type"] == "swap":
                if not self.swap_with_pool(i["pool"], coin, i["out"]["amount"]):
                    logging.error(f"Fail to exec instruction: {i}")
                    return False
            elif i["type"] == "dex":
                pool_asset = i["pool"]
                if not coin.asset.is_bnb():
                    pool_asset = coin.asset
                _, ok = self.swap_with_dex(pool_asset, coin, i["price"])
                if not ok:
                    logging.error(f"Failed DEX swap: {i}")
                    return False
            else:
                logging.error(f"invalid instruction type: {i['type']}")
                return False
        return True

    def raise_for_non_bnb(self, coin):
        if coin.asset != BNB:
            raise Exception(f"must start with BNB: got {coin}")

    def collect_funds(self):
        """
        collect all non-bnb funds and dex-exchange them to BNB
        """
        balance = self.binance.balance(self.wallet.address)
        for b in balance:
            b.asset = Asset(b.asset)
            if b.asset != "BNB.BNB":
                # skip if we have don't have enough for lot size
                if self.binance_dex.round_down_lot_size(b.asset, b.amount) <= 0:
                    continue
                logging.info(f"Collecting {b} --> BNB.BNB")
                order_book = self.binance_dex.get_order_book(b.asset)
                if len(order_book.bids) == 0:
                    continue
                amt = min(order_book.bids[0].amount * 1e8, b.amount)
                price = order_book.bids[0].price
                logging.info(f"DEX TRADE: {amt} {b.asset} @ {price}")
                self.swap_with_dex(b.asset, Coin(b.asset, amt), price)

    def swap_with_dex(self, pool_asset, coin, price):
        """
        Asset --> BNB or BNB --> Asset
        """
        if isinstance(pool_asset, str):
            pool_asset = Asset(pool_asset)

        if self.binance_dex.round_down_lot_size(pool_asset, coin.amount) <= 0:
            return Coin(pool_asset, 0), False

        market = self.binance_dex.get_market(pool_asset)
        if market is None:
            return Coin(pool_asset, 0), False

        try:
            resp = self.binance_dex.limit_order(pool_asset, price, coin)
        except Exception as e:
            logging.error(e)
            return Coin(pool_asset, 0), False

        if resp is None:
            return Coin(pool_asset, 0), False

        logging.info(f"Swapping with Dex ({pool_asset}): {coin}")
        data = json.loads(resp[0]["data"])
        result = self.binance_dex.wait_for_order(data["order_id"])
        if not result:
            # cancel order
            self.binance_dex.cancel_order(data["order_id"], pool_asset.get_symbol())
            return Coin(pool_asset, 0), False
        order = self.binance_dex.get_order(data["order_id"])
        return Coin(order["symbol"], float(order["quantity"]) * price), result

    def swap_with_pool(self, pool_asset, coin, trade_target):
        """
        Asset --> Pool --> Asset
        """
        if isinstance(pool_asset, str):
            pool_asset = Asset(pool_asset)

        logging.info(f"Swapping with Pool ({pool_asset}): {coin}")
        target = pool_asset if coin.asset != pool_asset else Asset("BNB.RUNE-B1A")
        resp = self.binance.transfer(
            self.wallet.address,
            self.thorchain_address,
            coin.asset.split(".")[1],
            int(coin.amount),
            f"SWAP:{target}::{trade_target}",
        )
        result = self.thorchain.wait_for_swap(resp[0]["hash"])
        if not result:
            return 0, False
        tx = self.thorchain.get_transaction(resp[0]["hash"])  # get the out hash
        tx = self.binance.get_transaction(tx["out_hashes"][0])
        coin = tx["outputs"][0]["coins"][0]
        return Coin(coin["denom"], coin["amount"]), result


class MockTransit:
    """
    A class to simulate the movement of funds, always starting with BNB, flowing through
    the DEX and THORChain to get a resulting amount of BNB
    """

    def __init__(self, balance, pool, dex, rune_order_book, asset_order_book=None):
        self.pool = pool
        self.binance_dex = dex
        self.balance = balance
        self.rune_order_book = rune_order_book
        self.asset_order_book = asset_order_book

        if pool.asset != BNB and asset_order_book is None:
            raise Exception(
                f"transit requires both order books if the pool is not BNB: {pool.asset}"
            )

    def raise_for_non_bnb(self, coin):
        if coin.asset != BNB:
            raise Exception(f"must start with BNB: got {coin}")

    def dex_ratio(self):
        rune_dex_ratio = self.rune_order_book.ratio()
        asset_dex_ratio = self.asset_order_book.ratio()
        if asset_dex_ratio == 0:
            return rune_dex_ratio
        market = self.binance_dex.get_market(self.pool.asset)
        if market is None:
            return 0
        if market["base_asset_symbol"] == "BNB":
            return rune_dex_ratio / (1 / float(asset_dex_ratio))
        elif market["quote_asset_symbol"] == "BNB":
            return rune_dex_ratio / float(asset_dex_ratio)
        else:
            return 0

    def evaluate_pool(self):
        """
        evaluates the pool to see how much asset is ideal to balance the pool
        relative to the order book
        """
        order_book = (
            self.rune_order_book
            if self.pool.asset == "BNB.BNB"
            else self.asset_order_book
        )
        pool_ratio = float(self.pool.balance_rune) / self.pool.balance_asset  # in asset
        dex_ratio = order_book.ratio()
        if dex_ratio == 0:
            raise Exception("cannot evaluate pool, dex ratio is zero")

        if pool_ratio > dex_ratio:
            # too much rune
            return (
                self.pool.asset,
                min(
                    int((self.pool.balance_rune / dex_ratio) - self.pool.balance_asset),
                    self.pool.balance_asset,
                ),
            )
        else:
            # too little rune
            return (
                Asset("BNB.RUNE-B1A"),
                min(
                    int((self.pool.balance_asset * dex_ratio) - self.pool.balance_rune),
                    self.pool.balance_rune,
                ),
            )

    def swap_with_dex(self, in_coin, book):
        """
        Asset --> BNB or BNB --> Asset
        """
        instruction = {
            "type": "dex",
            "pool": self.pool.asset,
            "in": in_coin.to_binance_fmt(),
        }

        market = self.binance_dex.get_market(book)
        if market is None:
            return Coin(BNB, 0), instruction

        order_type = "sell"
        if market["base_asset_symbol"] == "BNB" and not in_coin.asset.is_bnb():
            order_type = "buy"
        elif market["quote_asset_symbol"] == "BNB" and in_coin.asset.is_bnb():
            order_type = "buy"

        if in_coin.asset.is_bnb():
            if book == RUNE:
                amt, cost, price = self.rune_order_book.market_buy_order(in_coin.amount)
                out_coin = Coin(RUNE, amt)
            else:
                if order_type == "sell":
                    amt, cost, price = self.asset_order_book.market_sell_order(
                        in_coin.amount
                    )
                    out_coin = Coin(self.pool.asset, amt)
                else:
                    amt, cost, price = self.asset_order_book.market_buy_order(
                        in_coin.amount
                    )
                    out_coin = Coin(self.pool.asset, amt)
        else:
            if in_coin.asset == RUNE:
                amt, cost, price = self.rune_order_book.market_sell_order(
                    in_coin.amount
                )
                out_coin = Coin(BNB, amt)
            else:
                if order_type == "sell":
                    amt, cost, price = self.asset_order_book.market_sell_order(
                        in_coin.amount
                    )
                    out_coin = Coin(BNB, amt)
                else:
                    amt, cost, price = self.asset_order_book.market_buy_order(
                        in_coin.amount
                    )
                    out_coin = Coin(BNB, amt)

        # check that we have enough for the lot size
        if in_coin.asset == BNB:
            if self.binance_dex.round_down_lot_size(RUNE, in_coin.amount) <= 0:
                out_coin = Coin(RUNE, 0)
        else:
            if (
                self.binance_dex.round_down_lot_size(self.pool.asset, in_coin.amount)
                <= 0
            ):
                out_coin = Coin(BNB, 0)

        instruction["out"] = out_coin.to_binance_fmt()
        instruction["price"] = price
        instruction["cost"] = cost

        return out_coin, instruction

    def swap_with_pool(self, coin):
        """
        Asset --> Pool --> Asset
        """
        instruction = {
            "type": "swap",
            "pool": self.pool.asset,
            "in": coin.to_binance_fmt(),
        }
        amt = self.pool._calc_asset_emission(coin.amount, coin.asset)
        if coin.asset == RUNE:
            coin = Coin(self.pool.asset, amt)
        else:
            coin = Coin(RUNE, amt)
        instruction["out"] = coin.to_binance_fmt()
        return coin, instruction

    def clockwise(self, coin):
        """
        Send BNB clockwise, which sends ASSET into the THORChain pool
        """
        instructions = []
        self.raise_for_non_bnb(coin)

        if self.pool.asset != BNB:
            coin, instruction = self.swap_with_dex(coin, self.pool.asset)
            instructions.append(instruction)

        coin, instruction = self.swap_with_pool(coin)
        instructions.append(instruction)
        coin, instruction = self.swap_with_dex(coin, RUNE)
        instructions.append(instruction)
        self.raise_for_non_bnb(coin)
        return instructions

    def counter_clockwise(self, coin):
        """
        Send BNB counter clockwise, which sends RUNE into the THORChain pool
        """
        instructions = []
        self.raise_for_non_bnb(coin)

        coin, instruction = self.swap_with_dex(coin, RUNE)
        instructions.append(instruction)
        coin, instruction = self.swap_with_pool(coin)
        instructions.append(instruction)
        if self.pool.asset != BNB:
            coin, instruction = self.swap_with_dex(coin, self.pool.asset)
            instructions.append(instruction)
        self.raise_for_non_bnb(coin)
        return instructions
