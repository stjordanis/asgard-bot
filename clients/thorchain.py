import time
import logging

from util.common import HttpClient, Asset


class Thorchain(HttpClient):
    """
    A local simple implementation of thorchain chain
    """

    chain = "THOR"

    def get_transaction(self, hash_id):
        return self.fetch("/thorchain/tx/" + hash_id)

    def swap_complete(self, hash_id):
        data = self.get_transaction(hash_id)
        return data["status"].lower() == "done"

    def wait_for_swap(self, hash_id, timeout=300):
        for x in range(0, timeout):
            try:
                if self.swap_complete(hash_id):
                    return True
            except Exception as e:
                pass
            time.sleep(1)
        return False

    def asgard_address(self):
        data = self.fetch("/thorchain/pool_addresses")
        for d in data["current"]:
            if d["chain"] == "BNB":
                return d["address"]
        return ""

    def halted(self):
        data = self.fetch("/thorchain/pool_addresses")
        for d in data["current"]:
            if d["chain"] == "BNB":
                return d["halted"]
        return True

    def list_pools(self):
        return self.fetch("/thorchain/pools")


class Pool:
    def __init__(self, asset, rune_bal, asset_bal, status="Enabled"):
        self.asset = Asset(asset)
        self.balance_rune = rune_bal
        self.balance_asset = asset_bal
        self.status = status

    def asset_price(self):
        return float(self.balance_rune) / self.balance_asset

    def pool_price(self):
        return 1 / (float(self.balance_rune) / self.balance_asset)

    def _get_XY(self, fromRune):
        if fromRune:
            return self.balance_rune, self.balance_asset
        else:
            return self.balance_asset, self.balance_rune

    def _calc_liquidity_fee(self, x, fromRune=False):
        """
        Calculate the liquidity fee from a trade
        ( x^2 *  Y ) / ( x + X )^2
        """
        X, Y = self._get_XY(fromRune)
        return int(float((x ** 2) * Y) / float((x + X) ** 2))

    def _calc_trade_slip(self, x, fromRune=False):
        """
        Calculate the trade slip from a trade
        expressed in basis points (10,000)
        x * (2*X + x) / (X * X)
        """
        X, Y = self._get_XY(fromRune)
        trade_slip = 10000 * (x * (2 * X + x) / (X ** 2))
        return int(round(trade_slip))

    def _calc_asset_emission(self, x, asset):
        """
        Calculates the amount of coins to be emitted in a swap
        ( x * X * Y ) / ( x + X )^2
        """
        X, Y = self._get_XY(asset == Asset("BNB.RUNE-B1A"))
        return int((x * X * Y) / (x + X) ** 2)

    def __repr__(self):
        return f"<Pool {self.asset} ({self.status}) Rune {self.balance_rune}/{self.balance_asset} Asset>"

    def __str__(self):
        return f"Pool: {self.asset} ({self.status}) Rune {float(self.balance_rune) / 1e8}/{float(self.balance_asset) / 1e8} Asset"

    @classmethod
    def from_dict(cls, value):
        return cls(
            value["asset"],
            int(value["balance_rune"]),
            int(value["balance_asset"]),
            value["status"],
        )
