import unittest

from util.common import Asset
from clients.binance_dex import BinanceDex

BNB = Asset("BNB.BNB")
RUNE = Asset("BNB.RUNE-B1A")
AVA = Asset("BNB.AVA-645")
BUSD = Asset("BNB.BUSD-BD1")


class TestBinanceDex(unittest.TestCase):
    def test_dex(self):
        dex = BinanceDex()
        dex.markets = [
            {
                "base_asset_symbol": "BNB",
                "list_price": "0.29500000",
                "lot_size": "0.10000000",
                "quote_asset_symbol": "BUSD-BD1",
                "tick_size": "0.00000100",
            },
            {
                "base_asset_symbol": "RUNE-B1A",
                "list_price": "0.00065000",
                "lot_size": "0.10000000",
                "quote_asset_symbol": "BNB",
                "tick_size": "0.00000010",
            },
        ]
        # test get symbol
        self.assertEqual(dex.get_symbol(RUNE), "RUNE-B1A_BNB")
        self.assertEqual(dex.get_symbol(BUSD), "BNB_BUSD-BD1")

        # test round down
        self.assertEqual(dex.round_down_lot_size(BUSD, 555.55 * 1e8), 555.5)
