import unittest
import json

from util.common import Asset, Coin, OrderBook, Order

RUNE = Asset("BNB.RUNE-B1A")


class TestAsset(unittest.TestCase):
    def test_constructor(self):
        asset = Asset("BNB.BNB")
        self.assertEqual(asset, "BNB.BNB")
        asset = Asset("BNB")
        self.assertEqual(asset, "BNB.BNB")
        asset = Asset(RUNE)
        self.assertEqual(asset, RUNE)
        asset = Asset(RUNE)
        self.assertEqual(asset, RUNE)
        asset = Asset("BNB.LOK-3C0")
        self.assertEqual(asset, "BNB.LOK-3C0")

    def test_get_symbol(self):
        asset = Asset("BNB.BNB")
        self.assertEqual(asset.get_symbol(), "BNB")
        asset = Asset(RUNE)
        self.assertEqual(asset.get_symbol(), RUNE.get_symbol())
        asset = Asset("LOK-3C0")
        self.assertEqual(asset.get_symbol(), "LOK-3C0")

    def test_get_chain(self):
        asset = Asset("BNB.BNB")
        self.assertEqual(asset.get_chain(), "BNB")
        asset = Asset(RUNE)
        self.assertEqual(asset.get_chain(), RUNE.get_chain())
        asset = Asset("LOK-3C0")
        self.assertEqual(asset.get_chain(), "BNB")

    def test_is_rune(self):
        asset = Asset("BNB.BNB")
        self.assertEqual(asset.is_rune(), False)
        asset = Asset(RUNE)
        self.assertEqual(asset.is_rune(), True)
        asset = Asset("LOK-3C0")
        self.assertEqual(asset.is_rune(), False)
        asset = Asset("RUNE")
        self.assertEqual(asset.is_rune(), True)

    def test_to_json(self):
        asset = Asset("BNB.BNB")
        self.assertEqual(asset.to_json(), json.dumps("BNB.BNB"))
        asset = Asset("BNB.LOK-3C0")
        self.assertEqual(asset.to_json(), json.dumps("BNB.LOK-3C0"))
        asset = Asset(RUNE)
        self.assertEqual(asset.to_json(), json.dumps(RUNE))


class TestCoin(unittest.TestCase):
    def test_constructor(self):
        coin = Coin("BNB.BNB", 100)
        self.assertEqual(coin.asset, "BNB.BNB")
        self.assertEqual(coin.amount, 100)
        coin = Coin("BNB.BNB")
        self.assertEqual(coin.asset, "BNB.BNB")
        self.assertEqual(coin.amount, 0)
        coin = Coin(RUNE, 1000000)
        self.assertEqual(coin.amount, 1000000)
        self.assertEqual(coin.asset, RUNE)

        coin = Coin(RUNE, 400_000 * 100000000)
        c = coin.__dict__
        self.assertEqual(c["amount"], 400_000 * 100000000)
        self.assertEqual(c["asset"], RUNE)

    def test_is_zero(self):
        coin = Coin("BNB.BNB", 100)
        self.assertEqual(coin.is_zero(), False)
        coin = Coin("BNB.BNB")
        self.assertEqual(coin.is_zero(), True)
        coin = Coin(RUNE, 0)
        self.assertEqual(coin.is_zero(), True)

    def test_eq(self):
        coin1 = Coin("BNB.BNB", 100)
        coin2 = Coin("BNB.BNB")
        self.assertNotEqual(coin1, coin2)
        coin1 = Coin("BNB.BNB", 100)
        coin2 = Coin("BNB.BNB", 100)
        self.assertEqual(coin1, coin2)
        coin1 = Coin("BNB.LOK-3C0", 100)
        coin2 = Coin(RUNE, 100)
        self.assertNotEqual(coin1, coin2)
        coin1 = Coin("BNB.LOK-3C0", 100)
        coin2 = Coin("BNB.LOK-3C0", 100)
        self.assertEqual(coin1, coin2)
        coin1 = Coin("LOK-3C0", 200)
        coin2 = Coin("LOK-3C0", 200)
        self.assertEqual(coin1, coin2)
        coin1 = Coin("RUNE")
        coin2 = Coin("RUNE")
        self.assertEqual(coin1, coin2)
        # check list equality
        list1 = [Coin("RUNE", 100), Coin("RUNE", 100)]
        list2 = [Coin("RUNE", 100), Coin("RUNE", 100)]
        self.assertEqual(list1, list2)
        list1 = [Coin("RUNE", 100), Coin("RUNE", 100)]
        list2 = [Coin("RUNE", 10), Coin("RUNE", 100)]
        self.assertNotEqual(list1, list2)
        # list not sorted are NOT equal
        list1 = [Coin("RUNE", 100), Coin("BNB.BNB", 200)]
        list2 = [Coin("BNB.BNB", 200), Coin("RUNE", 100)]
        self.assertNotEqual(list1, list2)
        self.assertEqual(sorted(list1), sorted(list2))
        # check sets
        list1 = [Coin("RUNE", 100), Coin("RUNE", 100)]
        self.assertEqual(len(set(list1)), 1)
        list1 = [Coin("RUNE", 100), Coin("RUNE", 10)]
        self.assertEqual(len(set(list1)), 2)

    def test_is_rune(self):
        coin = Coin("BNB.BNB")
        self.assertEqual(coin.is_rune(), False)
        coin = Coin(RUNE)
        self.assertEqual(coin.is_rune(), True)
        coin = Coin("LOK-3C0")
        self.assertEqual(coin.is_rune(), False)
        coin = Coin("RUNE")
        self.assertEqual(coin.is_rune(), True)

    def test_to_binance_fmt(self):
        coin = Coin("BNB.BNB")
        self.assertEqual(coin.to_binance_fmt(), {"denom": "BNB", "amount": 0})
        coin = Coin("RUNE", 1000000)
        self.assertEqual(coin.to_binance_fmt(), {"denom": "RUNE", "amount": 1000000})
        coin = Coin("LOK-3C0", 1000000)
        self.assertEqual(coin.to_binance_fmt(), {"denom": "LOK-3C0", "amount": 1000000})

    def test_str(self):
        coin = Coin("BNB.BNB")
        self.assertEqual(str(coin), "0.0_BNB.BNB")
        coin = Coin(RUNE, 1000000)
        self.assertEqual(str(coin), "0.01_" + RUNE)
        coin = Coin("BNB.LOK-3C0", 100000000)
        self.assertEqual(str(coin), "1.0_BNB.LOK-3C0")

    def test_repr(self):
        coin = Coin("BNB.BNB")
        self.assertEqual(repr(coin), "<Coin 0_BNB.BNB>")
        coin = Coin(RUNE, 1000000)
        self.assertEqual(repr(coin), f"<Coin 1,000,000_{RUNE}>")
        coin = Coin("BNB.LOK-3C0", 1000000)
        self.assertEqual(repr(coin), "<Coin 1,000,000_BNB.LOK-3C0>")

    def test_to_json(self):
        coin = Coin("BNB.BNB")
        self.assertEqual(coin.to_json(), '{"asset": "BNB.BNB", "amount": 0}')
        coin = Coin(RUNE, 1000000)
        self.assertEqual(coin.to_json(), '{"asset": "' + RUNE + '", "amount": 1000000}')
        coin = Coin("BNB.LOK-3C0", 1000000)
        self.assertEqual(coin.to_json(), '{"asset": "BNB.LOK-3C0", "amount": 1000000}')

    def test_from_dict(self):
        value = {
            "asset": "BNB.BNB",
            "amount": 1000,
        }
        coin = Coin.from_dict(value)
        self.assertEqual(coin.asset, "BNB.BNB")
        self.assertEqual(coin.amount, 1000)
        value = {
            "asset": RUNE,
            "amount": "1000",
        }
        coin = Coin.from_dict(value)
        self.assertEqual(coin.asset, RUNE)
        self.assertEqual(coin.amount, 1000)


class TestOrder(unittest.TestCase):
    def test_order(self):
        order = Order(45, 0.00627400)

        self.assertEqual(order.get(45), 45)
        self.assertEqual(order.get(100), 45)
        self.assertEqual(order.get(10), 10)

        self.assertEqual(order.cost(45), 0.28233)
        self.assertEqual(order.cost(), 0.28233)
        self.assertEqual(order.cost(10), 0.06274)


class TestOrderBook(unittest.TestCase):

    RUNE_BINANCE_DATA = """
    { "bids": [ [ "0.00611900", "1000.00000000" ], [ "0.00611688", "50.00000000" ], [ "0.00611055", "21327.00000000" ], [ "0.00611013", "400.00000000" ], [ "0.00610888", "50.00000000" ], [ "0.00609000", "1085.00000000" ], [ "0.00606001", "250.00000000" ], [ "0.00601135", "2000.00000000" ], [ "0.00600000", "1301.00000000" ], [ "0.00592750", "1500.00000000" ] ], "asks": [ [ "0.00627400", "45.00000000" ], [ "0.00630994", "15258.00000000" ], [ "0.00630996", "61259.00000000" ], [ "0.00631000", "261883.00000000" ], [ "0.00633000", "22310.00000000" ], [ "0.00633820", "4235.00000000" ], [ "0.00633825", "311.00000000" ], [ "0.00633836", "300.00000000" ], [ "0.00633838", "2777.00000000" ], [ "0.00637800", "6721.00000000" ] ], "height": 90519035 }
    """

    def test_constructor(self):
        book = OrderBook.from_binance_dex_json(json.loads(self.RUNE_BINANCE_DATA))

        self.assertEqual(len(book.asks), 10)
        self.assertEqual(len(book.bids), 10)

    def test_market_buy_order(self):
        book = OrderBook.from_binance_dex_json(json.loads(self.RUNE_BINANCE_DATA))

        amt, cost, price = book.market_buy_order(0.28233 * 1e8)
        self.assertEqual(amt, 45.00000000000001 * 1e8)
        self.assertEqual(cost, 0.28233 * 1e8)
        self.assertEqual(price, 0.006274)

        amt, cost, price = book.market_buy_order(50 * 1e8)
        self.assertEqual(amt, 7924.261926420853 * 1e8)
        self.assertEqual(cost, 50 * 1e8)
        self.assertEqual(price, 0.00630994)

    def test_market_sell_order(self):
        book = OrderBook.from_binance_dex_json(json.loads(self.RUNE_BINANCE_DATA))

        amt, cost, price = book.market_sell_order(1000.00000000 * 1e8)
        self.assertEqual(amt, 6.119000000000001 * 1e8)
        self.assertEqual(cost, 1000.00000000 * 1e8)
        self.assertEqual(price, 0.006119)

        amt, cost, price = book.market_sell_order(1010 * 1e8)
        self.assertEqual(amt, 6.180168800000001 * 1e8)
        self.assertEqual(cost, 1010 * 1e8)
        self.assertEqual(price, 0.006119)


if __name__ == "__main__":
    unittest.main()
