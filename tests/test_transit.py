import unittest
import json

from util.common import Coin, OrderBook, Asset
from clients.thorchain import Pool
from clients.binance_dex import BinanceDex
from transit import MockTransit

BNB = Asset("BNB.BNB")
RUNE = Asset("BNB.RUNE-B1A")
AVA = Asset("BNB.AVA-645")
RUNE_BINANCE_DATA = """
    { "bids": [ [ "0.00611900", "1000.00000000" ], [ "0.00611688", "50.00000000" ], [ "0.00611055", "21327.00000000" ], [ "0.00611013", "400.00000000" ], [ "0.00610888", "50.00000000" ], [ "0.00609000", "1085.00000000" ], [ "0.00606001", "250.00000000" ], [ "0.00601135", "2000.00000000" ], [ "0.00600000", "1301.00000000" ], [ "0.00592750", "1500.00000000" ] ], "asks": [ [ "0.00627400", "45.00000000" ], [ "0.00630994", "15258.00000000" ], [ "0.00630996", "61259.00000000" ], [ "0.00631000", "261883.00000000" ], [ "0.00633000", "22310.00000000" ], [ "0.00633820", "4235.00000000" ], [ "0.00633825", "311.00000000" ], [ "0.00633836", "300.00000000" ], [ "0.00633838", "2777.00000000" ], [ "0.00637800", "6721.00000000" ] ], "height": 90519035 }
    """
AVA_BINANCE_DATA = """
{ "bids": [ [ "0.02159140", "131.80000000" ], [ "0.02152810", "177.00000000" ], [ "0.02152080", "154.00000000" ], [ "0.02147890", "93.00000000" ], [ "0.02147150", "175.00000000" ], [ "0.02145980", "1250.40000000" ], [ "0.02138890", "187.00000000" ], [ "0.02138560", "157.00000000" ], [ "0.02138480", "177.00000000" ], [ "0.02138180", "1.00000000" ] ], "asks": [ [ "0.02170560", "321.90000000" ], [ "0.02173300", "88.00000000" ], [ "0.02174300", "145.00000000" ], [ "0.02176790", "171.00000000" ], [ "0.02178190", "170.00000000" ], [ "0.02179040", "26.50000000" ], [ "0.02190170", "202.00000000" ], [ "0.02190350", "173.00000000" ], [ "0.02192420", "278.00000000" ], [ "0.02192490", "293.00000000" ] ], "height": 91486829 }
"""

DEX = BinanceDex()
DEX.markets = [
    {
        "base_asset_symbol": "BNB",
        "list_price": "0.29500000",
        "lot_size": "0.10000000",
        "quote_asset_symbol": "BUSD-BD1",
        "tick_size": "0.00000100",
    },
    {
        "base_asset_symbol": "RUNE-B1A",
        "list_price": "0.00065000",
        "lot_size": "0.10000000",
        "quote_asset_symbol": "BNB",
        "tick_size": "0.00000010",
    },
    {
        "base_asset_symbol": "AVA-645",
        "list_price": "1.00000000",
        "lot_size": "0.10000000",
        "quote_asset_symbol": "BNB",
        "tick_size": "0.00000010",
    },
]


class TestTransit(unittest.TestCase):
    def test_init(self):
        pool = Pool("BNB.TOMBO", 2502000000000000, 10000000000000)
        rune_order_book = OrderBook()
        asset_order_book = OrderBook()

        transit = MockTransit(pool, DEX, rune_order_book, asset_order_book)
        self.assertEqual(transit.pool, pool)
        self.assertEqual(transit.rune_order_book, rune_order_book)
        self.assertEqual(transit.asset_order_book, asset_order_book)

    def test_ratio(self):
        pool = Pool("BNB.AVA-645", 100 * 1e8, 100 * 1e8)
        rune_order_book = OrderBook.from_binance_dex_json(json.loads(RUNE_BINANCE_DATA))
        asset_order_book = OrderBook.from_binance_dex_json(json.loads(AVA_BINANCE_DATA))
        transit = MockTransit(pool, DEX, rune_order_book, asset_order_book)
        ratio = transit.dex_ratio()
        self.assertEqual(ratio, 3.4936657790688295)

    def test_eval(self):
        pool = Pool("BNB.BNB", 100 * 1e8, 100 * 1e8)
        rune_order_book = OrderBook.from_binance_dex_json(json.loads(RUNE_BINANCE_DATA))
        asset_order_book = OrderBook.from_binance_dex_json(json.loads(AVA_BINANCE_DATA))
        transit = MockTransit(pool, DEX, rune_order_book)
        asset, amt = transit.evaluate_pool()
        self.assertEqual(asset, Asset("BNB.RUNE-B1A"))
        self.assertEqual(amt, 10000000000)

        pool.balance_asset = 10000000
        transit = MockTransit(pool, DEX, rune_order_book)
        asset, amt = transit.evaluate_pool()
        self.assertEqual(asset, Asset("BNB.BNB"))
        self.assertEqual(amt, 10000000)

        pool = Pool("BNB.BNB", 100 * 158 * 1e8, 100 * 1e8)
        transit = MockTransit(pool, DEX, rune_order_book)
        asset, amt = transit.evaluate_pool()
        self.assertEqual(asset, Asset("BNB.RUNE-B1A"))
        self.assertEqual(amt, 33814249979)

        pool = Pool("BNB.BNB", 100 * 164 * 1e8, 100 * 1e8)
        transit = MockTransit(pool, DEX, rune_order_book)
        asset, amt = transit.evaluate_pool()
        self.assertEqual(asset, Asset("BNB.BNB"))
        self.assertEqual(amt, 162260000)

        pool = Pool("BNB.AVA-677", 1000 * 1e8, 1000 * 1e8)
        transit = MockTransit(pool, DEX, rune_order_book, asset_order_book)
        asset, amt = transit.evaluate_pool()
        self.assertEqual(asset, Asset("BNB.RUNE-B1A"))
        self.assertEqual(amt, 100000000000)

        pool = Pool("BNB.AVA-677", 1000 * 60 * 1e8, 1000 * 1e8)
        transit = MockTransit(pool, DEX, rune_order_book, asset_order_book)
        asset, amt = transit.evaluate_pool()
        self.assertEqual(asset, Asset("BNB.AVA-677"))
        self.assertEqual(amt, 29891000000)

    def test_bnb_trading(self):
        rune_order_book = OrderBook.from_binance_dex_json(json.loads(RUNE_BINANCE_DATA))

        # too much BNB
        pool = Pool(BNB, 100000 * 1e8, 100000 * 1e8)
        transit = MockTransit(pool, DEX, rune_order_book)
        instructions = transit.counter_clockwise(Coin(BNB, 10 * 1e8))
        profit = instructions[-1]["out"]["amount"] - instructions[0]["in"]["amount"]
        self.assertEqual(profit, 152597922686)
        instructions = transit.clockwise(Coin(BNB, 10 * 1e8))
        profit = instructions[-1]["out"]["amount"] - instructions[0]["in"]["amount"]
        self.assertEqual(profit, -993882224)

        # too much RUNE
        pool = Pool(BNB, 100 * 170 * 1e8, 100 * 1e8)
        transit = MockTransit(pool, DEX, rune_order_book)
        instructions = transit.counter_clockwise(Coin(BNB, 1 * 1e8))
        profit = instructions[-1]["out"]["amount"] - instructions[0]["in"]["amount"]
        self.assertEqual(profit, -8345266)

        instructions = transit.clockwise(Coin(BNB, 1 * 1e8))
        profit = instructions[-1]["out"]["amount"] - instructions[0]["in"]["amount"]
        self.assertEqual(profit, 1973335)

        # balanced
        pool = Pool(BNB, 100 * 162 * 1e8, 100 * 1e8)
        transit = MockTransit(pool, DEX, rune_order_book)
        instructions = transit.counter_clockwise(Coin(BNB, 1 * 1e8))
        profit = instructions[-1]["out"]["amount"] - instructions[0]["in"]["amount"]
        self.assertEqual(profit, -3906925)
        instructions = transit.clockwise(Coin(BNB, 1 * 1e8))
        profit = instructions[-1]["out"]["amount"] - instructions[0]["in"]["amount"]
        self.assertEqual(profit, -2825410)

    def test_ava_trading(self):
        rune_order_book = OrderBook.from_binance_dex_json(json.loads(RUNE_BINANCE_DATA))
        asset_order_book = OrderBook.from_binance_dex_json(json.loads(AVA_BINANCE_DATA))

        # too much AVA
        pool = Pool(AVA, 100000 * 1e8, 100000 * 1e8)
        transit = MockTransit(pool, DEX, rune_order_book, asset_order_book)
        instructions = transit.counter_clockwise(Coin(BNB, 10 * 1e8))
        profit = instructions[-1]["out"]["amount"] - instructions[0]["in"]["amount"]
        self.assertEqual(profit, 2300445879)

        instructions = transit.clockwise(Coin(BNB, 10 * 1e8))
        profit = instructions[-1]["out"]["amount"] - instructions[0]["in"]["amount"]
        self.assertEqual(profit, -720790034)

        # too much RUNE
        pool = Pool(AVA, 100 * 70 * 1e8, 100 * 1e8)
        transit = MockTransit(pool, DEX, rune_order_book, asset_order_book)
        instructions = transit.counter_clockwise(Coin(BNB, 1 * 1e8))
        profit = instructions[-1]["out"]["amount"] - instructions[0]["in"]["amount"]
        self.assertEqual(profit, -95318526)
        instructions = transit.clockwise(Coin(BNB, 1 * 1e8))
        profit = instructions[-1]["out"]["amount"] - instructions[0]["in"]["amount"]
        self.assertEqual(profit, 824464444)

        # balanced
        pool = Pool(AVA, (100 / float(46)) * 162 * 1e8, 100 * 1e8)
        transit = MockTransit(pool, DEX, rune_order_book, asset_order_book)
        instructions = transit.counter_clockwise(Coin(BNB, 1 * 1e8))
        profit = instructions[-1]["out"]["amount"] - instructions[0]["in"]["amount"]
        self.assertEqual(profit, -53759216)
        instructions = transit.clockwise(Coin(BNB, 1 * 1e8))
        profit = instructions[-1]["out"]["amount"] - instructions[0]["in"]["amount"]
        self.assertEqual(profit, -53469526)


if __name__ == "__main__":
    unittest.main()
