#!/usr/bin/env python

import time
import os
import logging

from util.common import Asset, Coin, OrderBook

from config import Configuration
from transit import MockTransit, Transit
from clients.thorchain import Thorchain, Pool
from clients.binance import MockBinance, Binance
from clients.binance_dex import BinanceDex
from prometheus_client import start_http_server, Gauge, Counter

# Init logging
logging.basicConfig(
    format="%(asctime)s | %(levelname).4s | %(message)s",
    level=os.environ.get("LOGLEVEL", "INFO"),
)


class AsgardArb:
    def __init__(self, config):
        self.cfg = config
        self.rune = Asset("BNB.RUNE-B1A")

        self.thorchain_client = Thorchain(self.cfg.thorchain)
        self.thorchain_address = self.thorchain_client.asgard_address()
        self.binance_dex = BinanceDex()
        self.binance = Binance()
        self.my_bnb_address = self.binance.get_address()
        self.prometheus = {"arb_success": {}, "arb_fail": {}, "wallet": {}, "price": {}}

        if self.cfg.simulation:
            logging.info("Seeding mock binance...")
            self.my_bnb_address = "tbnb16tysqp36d5qmyxclmdtxewvmhgzxh85e3688sm"

            self.binance = MockBinance()
            self.binance.seed(self.my_bnb_address, "RUNE-B1A", 1000000000000000)
            self.binance.seed(self.my_bnb_address, "BNB", 1000000000000000)
            self.binance.seed(self.my_bnb_address, "CAN-677", 10000000000000000)
            self.binance.seed(self.my_bnb_address, "TOMOB-4BC", 10000000000000000)
            self.binance.seed(self.my_bnb_address, "BUSD-BD1", 10000000000000000)
            self.binance.seed(self.my_bnb_address, "TUSDB-888", 10000000000000000)

            asset = int(100000 * 1e8)
            self.binance.stake(
                self.my_bnb_address, self.thorchain_address, "BNB", asset * 80, asset,
            )
            self.binance.stake(
                self.my_bnb_address,
                self.thorchain_address,
                "CAN-677",
                asset,
                asset * 8,
            )
            # self.binance.stake(self.my_bnb_address, self.thorchain_address, "TOMOB-4BC", asset)
            # self.binance.stake(self.my_bnb_address, self.thorchain_address, "BUSD-BD1", asset)
            # self.binance.stake(self.my_bnb_address, self.thorchain_address, "TUSDB-888", asset)

        time.sleep(10)

    def run(self):
        while True:
            logging.info("=======================================")
            if self.thorchain_client.halted():
                logging.info(">>>> Trading is halted...")
                continue
            pools = self.thorchain_client.list_pools()
            if len(pools) == 0:
                logging.info(">>>> No pools in thorchain....")
            self.thorchain_address = self.thorchain_client.asgard_address()
            transit = Transit(self.thorchain_address, self.cfg.thorchain)

            self.binance_dex.update_markets()  # update markets routinely
            transit.binance_dex.markets = self.binance_dex.markets

            if not self.cfg.simulation:
                transit.collect_funds()  # return non-bnb funds back to BNB

            # get our balances
            balance = self.binance.balance(self.my_bnb_address)
            bnb_balance = Coin("BNB.BNB", 0)
            for b in balance:
                if b.asset == "BNB.BNB":
                    logging.info(f"BNB Balance: {b.amount / 1e8} BNB")
                    bnb_balance.amount = b.amount - (
                        0.5 * 1e8
                    )  # minus a little for gas
                if b.asset not in self.prometheus["wallet"]:
                    self.prometheus["wallet"][b.asset] = Gauge(
                        "wallet_" + b.asset.prometheus_name(),
                        "Wallet " + b.asset.prometheus_name(),
                    )
                self.prometheus["wallet"][b.asset].set(b.amount / 1e8)

            for raw in pools:
                time.sleep(0.2)  # avoiding the API rate limit of binance chain
                pool = Pool.from_dict(raw)

                logging.info(f">>>>>> {pool}")

                # skip the pool if its not enabled
                if pool.status.lower() != "enabled":
                    continue

                rune_order_book = self.binance_dex.get_order_book(Asset("BNB.RUNE-B1A"))
                asset_order_book = OrderBook()
                if pool.asset != Asset("BNB.BNB"):
                    asset_order_book = self.binance_dex.get_order_book(pool.asset)

                if len(rune_order_book.asks) == 0 or len(rune_order_book.bids) == 0:
                    logging.info("no rune order book")
                    continue
                if pool.asset != Asset("BNB.BNB") and (
                    len(asset_order_book.asks) == 0 or len(asset_order_book.bids) == 0
                ):
                    logging.info("no asset order book")
                    continue

                mock_transit = MockTransit(
                    balance, pool, self.binance_dex, rune_order_book, asset_order_book
                )
                dex_price = 1 / mock_transit.dex_ratio()

                diff = 100 * (
                    (pool.pool_price() - dex_price) / (pool.pool_price() + dex_price)
                )
                diff = round(diff, 4)
                logging.info(
                    f"Price: {diff}% = {pool.pool_price():0.8f} Pool vs {dex_price:0.8f} DEX"
                )
                if pool.asset not in self.prometheus["price"]:
                    self.prometheus["price"][pool.asset] = {
                        "pool": Gauge(
                            "price_pool_" + pool.asset.prometheus_name(),
                            "THORChain Pool Price " + b.asset.prometheus_name(),
                        ),
                        "dex": Gauge(
                            "price_dex_" + pool.asset.prometheus_name(),
                            "DEX Price " + b.asset.prometheus_name(),
                        ),
                        "diff": Gauge(
                            "price_diff_" + pool.asset.prometheus_name(),
                            "Price Differential " + b.asset.prometheus_name(),
                        ),
                    }
                self.prometheus["price"][pool.asset]["pool"].set(pool.pool_price())
                self.prometheus["price"][pool.asset]["dex"].set(dex_price)
                self.prometheus["price"][pool.asset]["diff"].set(diff)

                instructions = sim_swap(mock_transit, bnb_balance, self.cfg.max_profits)
                if len(instructions) == 0:
                    continue

                if self.cfg.simulation:
                    for instruct in instructions:
                        if instruct["type"] == "swap":
                            logging.info(
                                f"Swapping: {instruct['in']['amount'] / 1e8} {instruct['in']['denom']} --> {instruct['pool']}"
                            )
                            trade_target = int(instruct["out"]["amount"] * 0.98)
                            self.binance.transfer(
                                self.my_bnb_address,
                                self.thorchain_address,
                                Asset(instruct["in"]["denom"]).get_symbol(),
                                int(instruct["in"]["amount"]),
                                f"SWAP:BNB.{instruct['out']['denom']}::{trade_target}",
                            )
                else:
                    ok = transit.run(instructions)
                    if ok:
                        if pool.asset not in self.prometheus["arb_success"]:
                            self.prometheus["arb_success"][pool.asset] = Counter(
                                "arb_succesful_" + pool.asset.prometheus_name(),
                                "num of successful arb for "
                                + pool.asset.prometheus_name(),
                            )
                        self.prometheus["arb_success"][pool.asset].inc()
                    else:
                        if pool.asset not in self.prometheus["arb_fail"]:
                            self.prometheus["arb_fail"][pool.asset] = Counter(
                                "arb_fail" + pool.asset.prometheus_name(),
                                "num of fail arb for " + pool.asset.prometheus_name(),
                            )
                        self.prometheus["arb_fail"][pool.asset].inc()
                    bnb_balance.amount -= instructions[0]["in"]["amount"]

            time.sleep(6)  # average block time is 5.5 seconds


def div1e8(x):
    return float(x) / 1e8


def sim_swap(transit, bnb_balance, max_profits=True):
    min_bnb = 0.1 * 1e8
    start = 1
    end = 100000 + start

    target = bnb_balance.amount

    # test clockwise
    last_profit = 0
    last_instructions = []
    for i in range(start, end):
        instructions = transit.clockwise(Coin("BNB.BNB", target / float(i)))
        profit = (
            instructions[-1]["out"]["amount"] - instructions[0]["in"]["amount"]
        )  # end - start
        if max_profits:
            if profit > last_profit:
                last_profit = profit
                last_instructions = instructions
            else:
                break
        else:
            if (profit < last_profit or last_profit == 0) and profit >= min_bnb:
                last_profit = profit
                last_instructions = instructions

    if last_profit > 0:
        logging.info(f"Clockwise Profit found: {last_profit / 1e8}")
    if last_profit > min_bnb:
        logging.info(f"{last_instructions}")
        return last_instructions

    # test counter-clockwise
    last_profit = 0
    last_instructions = []
    for i in range(start, end):
        instructions = transit.counter_clockwise(Coin("BNB.BNB", target / float(i)))
        profit = (
            instructions[-1]["out"]["amount"] - instructions[0]["in"]["amount"]
        )  # end - start
        if max_profits:
            if profit > last_profit:
                last_profit = profit
                last_instructions = instructions
            else:
                break
        else:
            if (profit < last_profit or last_profit == 0) and profit >= min_bnb:
                last_profit = profit
                last_instructions = instructions

    if last_profit > 0:
        logging.info(f"Counter Clockwise Profit found: {last_profit / 1e8}")
    if last_profit > min_bnb:
        logging.info(f"{last_instructions}")
        return last_instructions

    logging.info("No instructions found")
    return []


def main():
    logging.info("Starting asgard arb....")
    config = Configuration()
    start_http_server(config.port)
    arb = AsgardArb(config)
    arb.run()


def test():
    """
    testing func to do some manual testing. Please ignore
    """
    cfg = Configuration()
    thorchain_client = Thorchain(cfg.thorchain)
    thorchain_address = thorchain_client.asgard_address()
    transit = Transit(thorchain_address, cfg.thorchain)
    transit.collect_funds()  # return non-bnb funds back to BNB


if __name__ == "__main__":
    main()
